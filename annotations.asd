;;;; annotations.asd
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(asdf:defsystem #:annotations
  :description "Describe annotations here"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "COOPERATIVE NON-VIOLENT PUBLIC LICENSE v5+"
  :version "0.0.1"
  :depends-on ("alexandria" "closer-mop")
  :serial t
  :components ((:module "code" :components
                        ((:file "package")
                         (:file "slot-protocol")
                         (:file "slot")
                         (:file "class")
                         (:file "mirror")
                         (:file "class-mirror")
                         (:file "macro")))))
