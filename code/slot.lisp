#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:annotations)

(defclass standard-annotated-object (annotated-object)
  ((%annotation-table :initform (make-hash-table :size 5)
                       :reader annotation-table
                       :type hash-table)))

(defclass standard-annotated-slot (c2mop:standard-direct-slot-definition
                                   standard-annotated-object)
  ())

(defmethod get-direct-annotation ((slot standard-annotated-object) key)
  (gethash key (annotation-table slot)))

(defmethod (setf get-direct-annotation) (new-value (slot standard-annotated-object) key)
  (setf (gethash key (annotation-table slot)) new-value))
