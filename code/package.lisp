;;;; package.lisp
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(defpackage #:annotations
  (:use #:cl)
  (:export
   ;; slot protocol
   #:annotated-slot
   #:get-direct-annotation
   ;; slot
   #:standard-annotated-slot
   ;; class-protocol
   #:annotated-class
   #:names-annotated-slot-alist
   ;; class
   #:standard-annotated-class
   ;; slot-mirror
   #:slot-mirror
   #:get-annotation
   #:slot-name
   ;; standard-slot-mirror
   #:standard-slot-mirror
   ;; class-mirro
   #:class-mirror
   #:slots
   ;; standard-class-mirror
   #:standard-class-mirror
   ;; macro
   #:annotating-class
   #:define-annotated-class
))
