#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:annotations)

(defclass slot-mirror ()
  ()
  (:documentation "This is specifically for mirroring annotations
not the functionality of the effective definition of slot."))

(defgeneric get-annotation (slot-mirror key))

(defgeneric slot-name (slot-mirror))

(defgeneric class-mirror (slot-mirror))

;;; TODO
;;; hash-table-iterator maphash etc.
;;; maybe that neeeds to be on an annoatable thing
;;; so it can be used for classes too.

;;; TODO
;;; if the current class's direct-slot-definition doesn't have
;;; an annotatable-slot in the top level, and you try setf
;;; theres hould be a condition, we shouldn't allow that.
;;; unelss idk, we want to be able to reach the topmost.

;;; standard-annotated-object-mirror

(defclass annotated-object-mirror () ())

(defclass standard-annotated-object-mirror (annotated-object-mirror)
  ())

(defgeneric annotation-precedence-list (standard-annotated-object-mirror))

(defgeneric reflection-annotatable-p (standard-annotated-object-mirror))

(defmethod get-annotation ((object-mirror standard-annotated-object-mirror) key)
  "well, this works at least"
  (let ((result-annotation nil)
        (result-annotation-present-p nil))
    (loop :for slot :in (annotation-precedence-list object-mirror)
          :unless result-annotation-present-p
            :do (multiple-value-bind (annotation presentp)
                    (get-direct-annotation slot key)
                  (when presentp
                    (setf result-annotation annotation)
                    (setf result-annotation-present-p presentp))))
    (values result-annotation result-annotation-present-p)))

;;; standard-slot-mirror

(defclass standard-slot-mirror (standard-annotated-object-mirror)
  ((%slot-precedence-list :initarg :slot-precedence-list
                          :reader slot-precedence-list
                          :reader annotation-precedence-list
                          :type list :initform '())
   (%slot-name :initarg :slot-name :reader slot-name
               :type symbol)

   (%class-mirror :initarg :class-mirror :reader class-mirror)))

(defmethod reflection-annotatable-p ((slot-mirror standard-slot-mirror))
  (typep (reflected-class (class-mirror slot-mirror)) 'annotated-object))

(defmethod (setf get-annotation) (new-value
                                  (object-mirror standard-annotated-object-mirror)
                                  key)
  "it should be an error unless "
  (unless (reflection-annotatable-p object-mirror)
    (error "Are you sure you're doing this right? The closest class in the
class precedence list is not annotatable?"))
  (let ((target-direct-annotatable-object
          (first (annotation-precedence-list object-mirror))))
    (setf (get-direct-annotation target-direct-annotatable-object key)
          new-value)))

;;; mirror

#+ (or)
(defgeneric mirror (object mirror-class &rest initargs)
  (:method (object (mirror-class symbol) &rest initargs)
    (apply #'mirror-1 object mirror-class (find-class mirror-class) initargs))
  (:method (object (mirror-class c2mop:standard-class) &rest initargs)
    (apply #'mirror-1 (class-name mirror-class) mirror-class initargs)))

#+ (or)
(defgeneric mirror-1 (object mirror-class-name mirror-class &rest initargs)
  (:method (object mirror-class-name mirror-class &rest initargs)
    (declare (ignore mirror-class-name))
    (apply #'make-instance mirror-class initargs)))
