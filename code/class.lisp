#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:annotations)

(defgeneric names-annotated-slot-alist (annotated-class)
  (:documentation "an alist of annotated-slot lists
(slot-name . list-of-annotated-slots-in-order-of-inheritance-most-specific-first
used by the standard-slot-mirror"))

(defgeneric compute-names-annotated-slot-alist (annotated-class)
  (:documentation "Used for the standard-class-mirror"))

(defgeneric annotated-super-classes (annotated-class))

(defclass standard-annotated-class (c2mop:standard-class
                                    standard-annotated-object)
  ((%annotated-slots :reader names-annotated-slot-alist
                     :type list
                     :initform '())

   (%annotated-super-classes :reader annotated-super-classes
                             :type list
                             :initform '())))

(defmethod c2mop:direct-slot-definition-class ((class standard-annotated-class)
                                               &rest initargs)
  "I'm unsure whether we want to allow slots not to be annotatable on this level
I think it would be confusing/unecessary to make it optional. "
  (declare (ignore class initargs))
  (find-class 'standard-annotated-slot))

(defmethod compute-names-annotated-slot-alist ((class standard-annotated-class))
  (let ((names-annotated-slot-alist '()))
    (dolist (c (reverse (c2mop:class-precedence-list class)))
      (when (typep c 'annotated-object)
        (dolist (slot (c2mop:class-direct-slots c))
          (let* ((slot-name (c2mop:slot-definition-name slot))
                 (entry (assoc slot-name names-annotated-slot-alist)))
            (if entry
                (push slot (cdr entry))
                (push (list slot-name slot) names-annotated-slot-alist))))))
    names-annotated-slot-alist))

(defmethod c2mop:finalize-inheritance ((class standard-annotated-class))
  (prog1 (call-next-method)
    (setf (slot-value class '%annotated-slots)
          (compute-names-annotated-slot-alist class))))

(defmethod c2mop:validate-superclass ((class standard-annotated-class)
                                      (super c2mop:standard-class))
  (declare (ignore class super))
  t)

(defmethod c2mop:validate-superclass ((class c2mop:standard-class)
                                      (super standard-annotated-class))
  (declare (ignore class super))
  t)
