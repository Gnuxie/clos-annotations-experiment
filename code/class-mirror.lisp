#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:annotations)

(defclass class-mirror (annotated-object-mirror)
  ()
  (:documentation "This is just for annotations, not general introspection."))

(defgeneric slots (class-mirror)
  (:documentation "Return a list of the slot-mirrors for this class."))

(defclass standard-class-mirror (class-mirror standard-annotated-object-mirror)
  ((%reflected-class :initarg :reflected-class :reader reflected-class)))

;;; TODO
;;; some of the information e.g. closest-annotated-class could be cached in
;;; the annotated-isntance mixin, but this is compolex to setup.
;;; it could also be cached in the mirror itself, and set it up when the mirror
;;; is constructed.
(defmethod slots ((class-mirror standard-class-mirror))
  (let ((closest-annotated-class
          (find-if (lambda (c) (typep c 'annotated-object))
                   (c2mop:class-precedence-list (reflected-class class-mirror)))))
    (loop :for slot-entry :in (names-annotated-slot-alist closest-annotated-class)
          :collect (make-instance 'standard-slot-mirror
                                  :slot-name (car slot-entry)
                                  :slot-precedence-list (cdr slot-entry)
                                  :class-mirror class-mirror))))
(defmethod reflection-annotatable-p ((class-mirror standard-class-mirror))
  (typep (reflected-class class-mirror) 'annotated-object))

(defmethod annotation-precedence-list ((class-mirror standard-class-mirror))
  (annotated-super-classes (reflected-class class-mirror)))
