#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:annotations)

(defun %ensuring-slot-exists-insert-annotation (class-slots class slot-name
                                                slot-annotation-pairs)
  (loop :for (key value) :on slot-annotation-pairs :by #'cddr
        :do (let ((slot-entry (find slot-name class-slots :key #'slot-name)))
             (cond (slot-entry
                    (setf (get-annotation slot-entry key) value))
                   (t (error "No slot named ~a for class ~a" slot-name class))))))

(defmacro annotating-class ((class-name &key (class-mirror-class 'standard-class-mirror))
                            slot-annotations &rest class-annotations)
  `(progn
     (let* ((class (find-class ',class-name))
            (class-mirror (make-instance ',class-mirror-class :reflected-class class))
            (class-slots (slots class-mirror)))
       ,@(loop :for annotation :in slot-annotations
               :collect (destructuring-bind (slot-name &rest slot-annotation-pairs)
                            annotation
                          (let ((passed-annotation-pairs
                                  `(list
                                    ,@(loop :for (key value) :on slot-annotation-pairs :by #'cddr
                                            :collect `(cons ',key ,value)))))
                            `(%ensuring-slot-exists-insert-annotation
                              class-slots class ',slot-name ,passed-annotation-pairs))))
       ,@(loop :for (key value) :on class-annotations :by #'cddr
               :collect `(setf (get-annotation class-mirror ',key)
                               ,value)))))

(defmacro define-annotated-class (class-name direct-superclasses (&body slot-options)
                                  (&body class-annotations)
                                  (&key (class-mirror-class 'standard-class-mirror))
                                  &rest class-options)
  (let ((class-slot-options
          (loop :for option :in slot-options
                :collect (first option))))
    `(progn
       (defclass ,class-name ,direct-superclasses
         ,class-slot-options
         ,@ (let ((metaclass (assoc :metaclass class-options :key #'car)))
              (if metaclass
                  class-options
                  (list* '(:metaclass standard-annotated-class) class-options))))
       (c2mop:finalize-inheritance (find-class ',class-name))
       (annotating-class (,class-name :class-mirror-class ,class-mirror-class)
                         ,(loop :for slot-option :in slot-options
                                :collect (destructuring-bind ((slot-name &rest defclass-options) &rest annotations)
                                             slot-option
                                           (declare (ignore defclass-options))
                                           `(,slot-name . ,annotations)))
                         . ,class-annotations))))
