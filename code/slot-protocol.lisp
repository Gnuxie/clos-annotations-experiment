#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:annotations)

(defclass annotated-object () ())

(defgeneric get-direct-annotation (annotated-slot key)
  (:documentation "This is only used by the standard-slot-mirror
should not be used under any other circumstance unless you want
inconsistent behavoir"))

(defgeneric (setf get-direct-annotation) (new-value object key))

;;; it's incorrect to have the mirror on a standard-slot-definition.
;;; it should only be accessible from the class annotation mirror.
;;; you should only be able to set/add annotations from the mirror
;;; this means that a class can keep effective definitions themselves
;;; for the annotation tables and imporove speeds etc.
