# annotations
### _Gnuxie <Gnuxie@protonmail.com>_

This is an experimental/incomplete project to create annotations for classes

There will be a follow up project

Here's a rambling about this

>So I've noticed that people keep writing adhoc 'annotations' for classes e.g. there are several libraries out there now that create a subclass of c2mop:standard-direct-slot-definition and add some slots which are basically used like 'annotations', they then create a metaclass to use their direct-slot-definition then override c2mop:compute-class-precedence-list (which is WRONG!) or initialize-instance + reinitalize-instance to insert a mixin into the C-P-L
>they then use the mixin to add their real library functionality and do what they actually wanted to in the first place
>so i started writing an annoation library that would take care of the first problem (I mean, if it turned out good enough), but i did this by also creating my own standard-direct-slot-definition and metaclass to use it, but I'm not sure if that's a good idea at all because it means if there's something else someone needs mop for, they have to learn the internals of the library and extend the metaclass / slot-definition
>you could do this without using a metaclass at all if you created a table to keep track of classes that are annotated (and their slots) and just hide it behind a 'mirror' protocol
>now this does mean you can't do small performance optimizations (like caching the direct slots in order of class precedence when looking for annotations on the classes) but generally you would consider these dynamic reflection calls to be slow anyways so I'm not sure that would be anything to even consider worrying about
>i say i started writing an annotation library, i wrote all of it and one of the problems it also has is that you can't just annotate any class. So yeah I think i'm gonna write it again now after dumping this here, rant/thought train is over
>this table solution is actually just way simpler, makes me wonder why i didn't just do it in the first place, smh

example usage inspired by json-mop/json-clos

```
(defpackage #:json-annotation
  (:use #:cl)
  (:export
   #:json-key
   #:json-type))

(defpackage #:json-annotation.test
  (:use #:cl)
  (:local-nicknames
   (#:a #:annotations)
   (#:ja #:json-annotation)))

(in-package #:json-annotation.test)

(a:define-annotated-class book ()
  (((title :initarg :title)
    ja:json-key "title")
   ((published-year :initarg :year)
    ja:json-key "year_published")
   ((fiction :initarg :fiction)
    ja:json-key "is_fiction"))
  () ())
```

## License

COOPERATIVE NON-VIOLENT PUBLIC LICENSE v5+


Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>
